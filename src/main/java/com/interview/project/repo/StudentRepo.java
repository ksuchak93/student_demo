package com.interview.project.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.interview.project.model.StudentDetails;

public interface StudentRepo extends JpaRepository<StudentDetails, String> {

	Optional<StudentDetails> findByUsername(String username);
}
