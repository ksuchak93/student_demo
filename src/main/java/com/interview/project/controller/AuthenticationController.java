package com.interview.project.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.interview.project.dto.UserDto;
import com.interview.project.model.AuthReq;
import com.interview.project.model.AuthRes;
import com.interview.project.model.User;
import com.interview.project.repo.UserRepo;
import com.interview.project.security.JwtTokenProvider;
import com.interview.project.service.StudentService;
import com.interview.project.service.UserService;

@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
public class AuthenticationController {
	
	 	@Autowired
	    AuthenticationManager authenticationManager;

	    @Autowired
	    JwtTokenProvider jwtTokenProvider;

	    @Autowired
	    UserRepo userRepo;
	    
	    @Autowired
	    private UserService userService;
	    
	    @Autowired
	    private StudentService studentService;
	    
	    @Autowired
	    PasswordEncoder passwordEncoder;
	    
	    @PostConstruct
	    public void initData() {
	    	final User user = new User();
	    	user.setUsername("admin");
	    	user.setPassword(this.passwordEncoder.encode("admin"));
	    	user.setRoles(java.util.Arrays.asList("ROLE_USER", "ROLE_ADMIN"));
	    	this.userRepo.save(user);
	    }
	    
	    

	    @PostMapping("/signin")
	    public AuthRes signin(@RequestBody AuthReq data) {

	        try {
	            final String username = data.getUsername();
	            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
	            final String token = jwtTokenProvider.createToken(username, this.userRepo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found")).getRoles());
	            final AuthRes res = new AuthRes();
	            res.setToken(token);
	            res.setUsername(username);
	            res.setRoles(this.userRepo.findByUsername(username).get().getRoles());
	            return res;
	        } catch (org.springframework.security.core.AuthenticationException e) {
	            throw new BadCredentialsException("Invalid username/password supplied");
	        }
	    }
	    
	    @PostMapping("/signup")
	    public AuthRes signUp(@RequestBody UserDto dto) {
	    	final User user = new User();
	    	user.setUsername(dto.getUser().getUsername());
	    	user.setPassword(this.passwordEncoder.encode(dto.getUser().getPassword()));
	    	user.setRoles(java.util.Arrays.asList("ROLE_USER"));
	    	this.userService.save(user);
	    	 final AuthRes res = new AuthRes();
	         res.setUsername(dto.getUser().getUsername());
	    	
	    	this.studentService.save(dto.getStudentDetails());
	    	return res;
	    	
	    }

}
