package com.interview.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.interview.project.model.StudentDetails;
import com.interview.project.service.StudentService;
import com.interview.project.service.UserService;

@RestController
@RequestMapping("/admin")
@CrossOrigin("*")
public class AdminController {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private UserService userService;

	@GetMapping("/students")
	public List<StudentDetails> getList(){
		return studentService.getAllStudents();		
	}
	
	@GetMapping("/remove/student/{username}")
	public void remove(@PathVariable(value = "username") String username) {
		userService.remove(username);
		studentService.remove(username);
	}
}
