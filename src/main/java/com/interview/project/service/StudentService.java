package com.interview.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interview.project.model.StudentDetails;
import com.interview.project.repo.StudentRepo;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepo studentRepo;
	
	public List<StudentDetails> getAllStudents() {
		return studentRepo.findAll();
	}
	
	public StudentDetails save(StudentDetails details) {
		if (studentRepo.findByUsername(details.getUsername()).isPresent()) {
			throw new RuntimeException("Username Already Exists");
		}
		return studentRepo.save(details);
	}
	
	public StudentDetails get(String username) {
		return studentRepo.getOne(username);
	}
	
	public void remove(String username) {
		studentRepo.deleteById(username);
	}
	
	public StudentDetails update(StudentDetails studentDetails) {
		final StudentDetails data = get(studentDetails.getUsername());
		if(data == null) {
			throw new RuntimeException("No details available");
		}
		return studentRepo.save(studentDetails);
	}

}
