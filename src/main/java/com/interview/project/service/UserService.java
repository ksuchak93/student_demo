package com.interview.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interview.project.model.User;
import com.interview.project.repo.UserRepo;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;

	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	public User save(User user) {
		if (userRepo.findByUsername(user.getUsername()).isPresent()) {
			throw new RuntimeException("Username Already Exists");
		}
		return userRepo.save(user);
	}

	public void remove(String username) {
		if (!userRepo.findByUsername(username).isPresent()) {
			throw new RuntimeException("Username Not Exists");
		}

		final User user = userRepo.findByUsername(username).get();
		userRepo.delete(user);

	}

}
