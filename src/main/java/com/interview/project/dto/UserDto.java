package com.interview.project.dto;

import org.springframework.security.core.userdetails.UserDetails;

import com.interview.project.model.StudentDetails;
import com.interview.project.model.User;

public class UserDto {
	
	private User user;
	
	private StudentDetails studentDetails;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public StudentDetails getStudentDetails() {
		return studentDetails;
	}

	public void setStudentDetails(StudentDetails studentDetails) {
		this.studentDetails = studentDetails;
	}

	
	
	

}
